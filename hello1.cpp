////////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - animalFarm0 - EE 205 - Spr 2022
///
/// @file hello1.cpp
/// @version 1.0
///
///
/// @author Bodie Collins < bodie@hawaii.edu>
/// @date  01 March 2022
///////////////////////////////////////////////////////////////////////

#include <iostream>
using namespace std;

int main() 
{
   cout<<"Hello World"<<endl ;
   return 0;
}
