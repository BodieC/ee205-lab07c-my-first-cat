////////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - animalFarm0 - EE 205 - Spr 2022
///
/// @file hello3.cpp
/// @version 1.0
///
///
/// @author Bodie Collins < bodie@hawaii.edu>
/// @date  01 Mar 2022
///////////////////////////////////////////////////////////////////////

#include <iostream>
using namespace std;

class Cat 
{
   public:

   void sayHello() 
      {
         cout<<"Meow"<<endl ;
      }
};

int main()
{
   Cat mycat;
   mycat.sayHello();
   return 0;
}

